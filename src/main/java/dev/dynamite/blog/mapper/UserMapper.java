package dev.dynamite.blog.mapper;

import dev.dynamite.blog.entity.User;
import dev.dynamite.blog.model.dto.RequestRegisterDto;
import dev.dynamite.blog.model.dto.ResponseRegisterDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User RequestRegisterDtoToUser(RequestRegisterDto requestRegisterDto);

    ResponseRegisterDto UserToResponseRegisterDto(User user);
}
