package dev.dynamite.blog.model.dto;

import lombok.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseRegisterDto {
    private Integer id;
    private String username;
    private String email;
}
