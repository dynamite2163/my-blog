package dev.dynamite.blog.model.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestRegisterDto {
    private String username;
    private String password;
    private String email;
}
