package dev.dynamite.blog.enums;

import org.springframework.http.HttpStatus;

public enum ResponseValue {

    // Success - 20x
    SUCCESS(HttpStatus.OK, "Success"),
    LOGIN_SUCCESS(HttpStatus.OK, "Login Success"),
    LOGOUT_SUCCESS( HttpStatus.OK, "Logout Success"),
    REGISTER_SUCCESS(HttpStatus.OK, "Register Success"),
    UPDATE_SUCCESS(HttpStatus.OK, "Update Success"),
    DELETE_SUCCESS(HttpStatus.OK, "Delete Success"),
    // Bad Request - 40x
    BAD_REQUEST(HttpStatus.BAD_REQUEST, "Bad Request"),
    UNAUTHORIZED(HttpStatus.UNAUTHORIZED, "Unauthorized"),
    FORBIDDEN(HttpStatus.FORBIDDEN, "Forbidden"),
    NOT_FOUND(HttpStatus.NOT_FOUND, "Not Found"),
    CONFLICT(HttpStatus.CONFLICT, "Conflict"),
    // Server Error - 50x
    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "Internal Server Error"),
    SERVICE_UNAVAILABLE(HttpStatus.SERVICE_UNAVAILABLE, "Service Unavailable");

    private HttpStatus code;
    private String message;
    ResponseValue(HttpStatus code, String message) {
        this.code = code;
        this.message = message;
    }

    public HttpStatus getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
