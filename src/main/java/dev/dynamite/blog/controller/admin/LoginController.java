package dev.dynamite.blog.controller.admin;

import dev.dynamite.blog.entity.User;
import dev.dynamite.blog.enums.ResponseValue;
import dev.dynamite.blog.exception.ResponseException;
import dev.dynamite.blog.mapper.UserMapper;
import dev.dynamite.blog.model.dto.RequestLoginDto;
import dev.dynamite.blog.model.dto.RequestRegisterDto;
import dev.dynamite.blog.model.dto.ResponseLoginDto;
import dev.dynamite.blog.model.dto.ResponseRegisterDto;
import dev.dynamite.blog.service.Impl.UserService;
import dev.dynamite.blog.util.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/cms/")
public class LoginController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private JwtUtils JwtUtils;
    @PostMapping("/login")
    public ResponseLoginDto login(@RequestBody RequestLoginDto requestLoginDto) throws ResponseException {
        User user = userService.findByUsernameAndPassword(requestLoginDto.getUsername(), requestLoginDto.getPassword());
        if(user == null){
            throw new ResponseException(ResponseValue.NOT_FOUND);
        }
        String token = JwtUtils.generateJwtToken(user.getUsername());
        return new ResponseLoginDto(token);
    }

    @PostMapping("/register")
    public ResponseRegisterDto register(@RequestBody RequestRegisterDto requestLoginDto) throws ResponseException {
        User user = userService.findByUsername(requestLoginDto.getUsername());
        if(user != null){
            throw new ResponseException(ResponseValue.CONFLICT);
        }else{
            user  = userMapper.RequestRegisterDtoToUser(requestLoginDto);
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userService.save(user);
            return userMapper.UserToResponseRegisterDto(user);
        }
    }
}
