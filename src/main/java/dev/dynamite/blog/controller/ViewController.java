package dev.dynamite.blog.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/view/")
public class ViewController {
    @GetMapping("/")
    public String index(){
        return null;
    }
}
