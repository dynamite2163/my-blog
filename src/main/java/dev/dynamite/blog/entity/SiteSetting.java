package dev.dynamite.blog.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "site_setting")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SiteSetting {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name="value")
    private String value;
    @Column(name="type")
    private int type;
}
