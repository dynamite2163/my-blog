package dev.dynamite.blog.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;

@Entity
@Table(name = "blog")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Blog {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="title")
    private String title;
    @Column(name="first_picture")
    private String firstPicture;
    @Column(name="content")
    private String content;
    @Column(name="description")
    private String description;
    @Column(name="is_published")
    private byte isPublished;
    @Column(name="is_recommended")
    private byte isRecommend;
    @Column(name="is_comment_enabled")
    private byte isCommentable;
    @Column(name="create_time")
    private Timestamp createTime;
    @Column(name="update_time")
    private Timestamp updateTime;
    @Column(name="views")
    private Integer views;
    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Category category;
    @Column(name="is_top")
    private byte isTop;
}
