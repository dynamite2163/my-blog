package dev.dynamite.blog.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;

@Entity
@Table(name = "comment")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Comment {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne()
    @JoinColumn(name = "blog_id", referencedColumnName = "id")
    private Blog blog;
    @ManyToOne()
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    private Comment comment;
    @Column(name = "user_name")
    private String userName;
    @Column(name = "email")
    private String email;
    @Column(name = "content")
    private String content;
    @Column(name = "create_time", columnDefinition = "TIMESTAMP")
    private Timestamp createTime;
    @Column(name = "is_admin_comment", columnDefinition = "TINYINT(1)")
    private Byte isAdminComment;
    @Column(name = "page")
    private Integer page;
}
