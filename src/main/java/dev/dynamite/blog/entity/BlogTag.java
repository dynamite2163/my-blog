package dev.dynamite.blog.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "blog_tag")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BlogTag {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name="blog_id", referencedColumnName = "id")
    private Blog blog;
    @ManyToOne
    @JoinColumn(name="tag_id", referencedColumnName = "id")
    private Tag tag;
}
