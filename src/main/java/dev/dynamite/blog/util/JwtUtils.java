package dev.dynamite.blog.util;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;

@Component
public class JwtUtils {
    @Value("${jwt.expiration}")
    private String JWT_EXPIRATION;
    @Value("${jwt.secretKey}")
    private String JWT_SECRETKEY;

    /**
     * check token is exits
     * @param token
     * @return
     */
    public Boolean TokenIsExits(String token){
        return token != null && !"".equals(token) && !"null".equals(token);
    }
    /**
     * get secret key
     * get keys
     * @return
     */
    private Key getKey() {
        return Keys.hmacShaKeyFor(Decoders.BASE64.decode(JWT_SECRETKEY));
    }
    /**
     * generate jwt token
     * @param subject
     * @return
     */
    public String generateJwtToken(String subject){
        return Jwts.builder()
                .setSubject(subject)
                .setExpiration(new Date(System.currentTimeMillis() + Long.parseLong(JWT_EXPIRATION)))
                .signWith(getKey(), SignatureAlgorithm.HS256)
                .compact();
    }
    /**
     * get subject
     * @param token
     * @return
     */
    public String getSubject(String token){
        return Jwts.parserBuilder()
                .setSigningKey(getKey())
                .build()
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
    }
    /**
     * get jwt from request
     * @param token
     * @return
     */
    public String getJwtFromRequest(String token){
        if (TokenIsExits(token) && token.startsWith("Bearer ")){
            return token.substring(7);
        }else{
            return "";
        }
    }
    /**
     * validate token
     * @param token
     * @return
     */
    public Boolean validateToken(String token) {
        try {
            Jwts.parserBuilder()
                    .setSigningKey(getKey())
                    .build()
                    .parseClaimsJws(token);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
