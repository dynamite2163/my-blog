package dev.dynamite.blog.exception;

import dev.dynamite.blog.enums.ResponseValue;
import dev.dynamite.blog.handler.model.BaseResponseBody;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ResponseException extends Exception{
    private HttpStatus httpStatus;
    private BaseResponseBody<?> baseResponseBody;

    public ResponseException(HttpStatus httpStatus, BaseResponseBody<?> baseResponseBody) {
        this.httpStatus = httpStatus;
        this.baseResponseBody = baseResponseBody;
    }

    public ResponseException(ResponseValue responseValue) {
        this.httpStatus = responseValue.getCode();
        this.baseResponseBody = new BaseResponseBody<>(responseValue);
    }
}
