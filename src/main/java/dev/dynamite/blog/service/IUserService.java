package dev.dynamite.blog.service;

import dev.dynamite.blog.entity.User;
import dev.dynamite.blog.exception.ResponseException;

public interface IUserService {
    User finalById(Integer id) throws ResponseException;
    User findByUsernameAndPassword(String username, String password) throws ResponseException;
    User findByUsername(String username) throws ResponseException;
    User save(User user);
}
