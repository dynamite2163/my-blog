package dev.dynamite.blog.service.Impl;

import dev.dynamite.blog.entity.User;
import dev.dynamite.blog.enums.ResponseValue;
import dev.dynamite.blog.exception.ResponseException;
import dev.dynamite.blog.model.vo.MyUserDetails;
import dev.dynamite.blog.repository.UserRepository;
import dev.dynamite.blog.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService implements IUserService, UserDetailsService {
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;
    @Override
    public User finalById(Integer id) throws ResponseException {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()){
            return user.get();
        }else{
            throw new ResponseException(ResponseValue.NOT_FOUND);
        }
    }

    @Override
    public User findByUsernameAndPassword(String username, String password) throws ResponseException {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isPresent() && passwordEncoder.matches(password, user.get().getPassword())){
            return user.get();
        }else{
            throw new ResponseException(ResponseValue.NOT_FOUND);
        }
    }

    @Override
    public User findByUsername(String username) throws ResponseException {
        Optional<User> user = userRepository.findByUsername(username);
        return user.orElse(null);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isPresent()){
            return new MyUserDetails(user.get());
        }else{
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
    }
}
