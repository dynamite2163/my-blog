package dev.dynamite.blog.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.dynamite.blog.enums.ResponseValue;
import dev.dynamite.blog.handler.model.BaseResponseBody;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.PrintWriter;

@Component
public class MyAuthenEntryPoint implements AuthenticationEntryPoint {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setContentType("application/json;charset=utf-8");
        ResponseValue responseValue = ResponseValue.UNAUTHORIZED;
        PrintWriter writer = response.getWriter();
        writer.write(objectMapper.writeValueAsString(new BaseResponseBody<>(responseValue)));
        writer.flush();
        writer.close();
    }
}
