package dev.dynamite.blog.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.dynamite.blog.enums.ResponseValue;
import dev.dynamite.blog.handler.model.BaseResponseBody;
import dev.dynamite.blog.service.Impl.UserService;
import dev.dynamite.blog.util.JwtUtils;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.io.PrintWriter;

@Component
public class JwtFilter extends OncePerRequestFilter {
    @Autowired
    private UserService UserService;
    @Autowired
    private JwtUtils JwtUtils;
    private static final ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = request.getHeader("Authorization");
        token = JwtUtils.getJwtFromRequest(token);
        if (JwtUtils.TokenIsExits(token) && JwtUtils.validateToken(token)) {
            try {
                String username = JwtUtils.getSubject(token);
                UserDetails userDetails = UserService.loadUserByUsername(username);
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, null);
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }catch(Exception e) {
                e.printStackTrace();
                response.setContentType("application/json;charset=utf-8");
                ResponseValue responseValue = ResponseValue.UNAUTHORIZED;
                PrintWriter writer = response.getWriter();
                writer.write(objectMapper.writeValueAsString(new BaseResponseBody<>(responseValue)));
                writer.flush();
                writer.close();
                return;
            }
        }
        filterChain.doFilter(request, response);
    }
}
