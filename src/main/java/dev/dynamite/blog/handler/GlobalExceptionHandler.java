package dev.dynamite.blog.handler;

import dev.dynamite.blog.exception.ResponseException;
import dev.dynamite.blog.handler.model.BaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice(basePackages = {"dev.dynamite.blog"})
public class GlobalExceptionHandler {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public BaseResponse<?> onInternalServerError(Exception e) {
        e.printStackTrace();
        return new BaseResponse<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ResponseException.class)
    @ResponseBody
    public BaseResponse<?> onResponseException(ResponseException e) {
        return new BaseResponse<>(e.getHttpStatus(), e.getBaseResponseBody());
    }
}
