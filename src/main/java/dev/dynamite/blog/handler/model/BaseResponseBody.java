package dev.dynamite.blog.handler.model;

import dev.dynamite.blog.enums.ResponseValue;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class BaseResponseBody<T> {
    private HttpStatus code;
    private String message;
    private T data;

    public BaseResponseBody() {
    }

    public BaseResponseBody(HttpStatus code, String message) {
        this.code = code;
        this.message = message;
        this.data = null;
    }

    public BaseResponseBody(HttpStatus code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public BaseResponseBody(ResponseValue responseValue, T data) {
        this(responseValue.getCode(), responseValue.getMessage(), data);
    }

    public BaseResponseBody(ResponseValue responseValue) {
        this(responseValue.getCode(), responseValue.getMessage());
    }
}
