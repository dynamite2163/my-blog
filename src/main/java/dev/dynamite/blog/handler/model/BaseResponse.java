package dev.dynamite.blog.handler.model;

import dev.dynamite.blog.enums.ResponseValue;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

public class BaseResponse<T> extends ResponseEntity<BaseResponseBody<T>> {
    public BaseResponse(HttpStatusCode status) {
        super(status);
    }

    public BaseResponse( HttpStatusCode status, BaseResponseBody<T> body) {
        super(body, status);
    }

    public BaseResponse(ResponseValue responseValue) {
        super(new BaseResponseBody<>(responseValue), responseValue.getCode());
    }

    public BaseResponse(ResponseValue responseValue, T data) {
        super(new BaseResponseBody<>(responseValue, data), responseValue.getCode());
    }
}
