use ngoctam;

# create table about
drop table if exists about;
create table about (
                       id int not null,
                       name varchar(255) null default null,
                       value nvarchar(255) null default null,
                       primary key (id)
) engine=InnoDB default charset=utf8;

# mokup data of table about
INSERT INTO about VALUES (1, 'title', 'Dynamite\'s Blog');
INSERT INTO about VALUES (2, 'musicId', '');
INSERT INTO about VALUES (3, 'content', '');
INSERT INTO about VALUES (4, 'commentEnabled', 'true');

# Create table category
drop table if exists category;
create table category (
    id int not null auto_increment,
    category_name varchar(255) null default null,
    primary key (id)
) engine=InnoDB default charset=utf8;

# Create table blog
drop table if exists blog;
create table blog (
    id bigint not null auto_increment,
    title varchar(255) not null default '',
    first_picture varchar(255) not null default '',
    content text not null,
    description text not null,
    is_published tinyint(1) not null default 0,
    is_recommended tinyint(1) not null default 0,
    is_comment_enabled tinyint(1) not null default 0,
    create_time timestamp not null default current_timestamp,
    update_time timestamp not null default current_timestamp,
    views int not null default 0,
    category_id int not null,
    is_top tinyint(1) not null default 0,
    primary key (id),
    foreign key (category_id) references category(id)
) engine=InnoDB default charset=utf8;

# create table tag
drop table if exists tag;
create table tag (
    id int not null auto_increment,
    tag_name varchar(255) not null default '',
    color varchar(255) not null default 'black',
    primary key (id)
) engine=InnoDB default charset=utf8;

# create table blog_tag
drop table if exists blog_tag;
create table blog_tag (
    id bigint not null auto_increment,
    blog_id bigint not null,
    tag_id int not null,
    primary key (id),
    foreign key (blog_id) references blog(id),
    foreign key (tag_id) references tag(id)
) engine=InnoDB default charset=utf8;

# create table comment
drop table if exists comment;
create table comment (
    id bigint not null auto_increment,
    blog_id bigint not null,
    parent_id bigint not null default 0,
    user_name varchar(255) not null default '',
    email varchar(255) not null default '',
    content text not null,
    create_time timestamp not null default current_timestamp,
    is_admin_comment tinyint(1) not null default 0,
    page int not null default 0, -- 0 : bình thươờng, 1: về bản thân, : 2:  về bạn bè
    primary key (id),
    foreign key (blog_id) references blog(id)
) engine=InnoDB default charset=utf8;

# create table friend
drop table if exists friend;
create table friend (
    id int not null auto_increment,
    name varchar(255) not null default '',
    avatar varchar(255) not null default '',
    description varchar(255) not null default '',
    link varchar(255) not null default '',
    primary key (id)
) engine=InnoDB default charset=utf8;

# create table site_setting
drop table if exists site_setting;
create table site_setting (
    id int not null auto_increment,
    name varchar(255) not null default '',
    value varchar(255) not null default '',
    type int not null default 0, -- 0: cài đặt cơ bản, 1: logo phân trang, 2: thẻ thông tin, 3: link bạn bè
    primary key (id)
) engine=InnoDB default charset=utf8;

# mokup data of table site_setting
INSERT INTO site_setting VALUES (1, 'blogName', 'Dynamite\'s Blog', 1);
INSERT INTO site_setting VALUES (2, 'webTitleSuffix', ' - Dynamite', 1);
INSERT INTO site_setting VALUES (3, 'footerImgTitle', 'Hi, I am Dynamite', 1);
INSERT INTO site_setting VALUES (4, 'footerImgUrl', '/img/qr.png', 1);
INSERT INTO site_setting VALUES (5, 'reward', '/img/reward.jpg', 1);
INSERT INTO site_setting VALUES (6, 'commentAdminFlag', 'Halu', 1);
INSERT INTO site_setting VALUES (7, 'avatar', '/img/avatar.jpg', 2);
INSERT INTO site_setting VALUES (8, 'name', 'Dynamite', 2);
INSERT INTO site_setting VALUES (9, 'rollText', '\"Code is Law；\",\"Law is Code.\"', 2);
INSERT INTO site_setting VALUES (10, 'github', 'https://github.com/trieungoctam', 2);
INSERT INTO site_setting VALUES (11, 'email', 'ss.optimus2003@gmail.com', 2);

# create table user_infor
drop table if exists user_infor;
create table user_infor (
                            id int not null auto_increment,
                            username varchar(255) not null default '',
                            password varchar(255) not null default '',
                            email varchar(255) not null default '',
                            primary key (id)
) engine=InnoDB default charset=utf8;

# mockup data of table user_infor
INSERT INTO user_infor VALUES (1, 'dynamite2003', '21062003', 'tamtn.dynamite.work@gmail.com');
